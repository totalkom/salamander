//nolint:gomnd
package live

import (
	"os"
	"os/signal"
	"syscall"
	"time"

	MQTT "github.com/eclipse/paho.mqtt.golang"
	"github.com/rs/zerolog/log"
	"gitlab.com/wobcom/salamander/pkg/mqtt"
)

func Execute(m *mqtt.Connection) {
	sigs := make(chan os.Signal, 1)
	done := make(chan bool, 1)

	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		sig := <-sigs

		log.Debug().Str("signal", sig.String()).Msg("caught signal")
		done <- true
	}()

	opts := MQTT.NewClientOptions().AddBroker(m.Server)
	opts.SetClientID("")
	opts.SetDefaultPublishHandler(m.MsgHandler)
	opts.SetUsername(m.Username)
	opts.SetPassword(m.Password)
	opts.SetKeepAlive(15 * time.Second)

	c := MQTT.NewClient(opts)
	if token := c.Connect(); token.Wait() && token.Error() != nil {
		panic(token.Error())
	}

	for _, channel := range m.Channel {
		if token := c.Subscribe(channel, 0, nil); token.Wait() && token.Error() != nil {
			log.Error().Caller().Msg(token.Error().Error())
			os.Exit(1)
		}

		log.Debug().Str("channel", channel).Msg("subscribed")
	}

	<-done

	for _, channel := range m.Channel {
		if token := c.Unsubscribe(channel); token.Wait() && token.Error() != nil {
			log.Error().Msg(token.Error().Error())
			os.Exit(1)
		}

		log.Debug().Str("channel", channel).Msg("unsubscribed")
	}

	c.Disconnect(250)

	if m.LogDir != "" {
		log.Debug().Msg("waiting for finnishing logfile writing...")
		m.WG.Wait()
	}

	log.Info().Msg("have a nice day!")
}
