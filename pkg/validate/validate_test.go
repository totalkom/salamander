// nolint: lll
package validate_test

import (
	"io/ioutil"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/wobcom/salamander/pkg/validate"
)

func TestValidFails(t *testing.T) {
	assert := assert.New(t)

	schema := `
{
  "$schema": "http://json-schema.org/draft-07/schema#",
  "$id": "https://wobcom.gitlab.io/iot-schemas/DeviceFeedback.json",
  "title": "Device / Device Feedback",
  "description": "A device feedback is mostly a device with some smiley feedback buttons and count how much it got pressed.",
  "type": "object",
  "properties": {
    "type": {
      "type": "string",
      "enum": ["DeviceFeedback"],
      "description": "device feedback type"
    },
    "red": {
      "type": "integer",
      "description": "mostly used for bad feedback"
    },
    "yellow": {
      "type": "integer",
      "description": "mostly used for medium feedback"
    },
    "green": {
      "type": "integer",
      "description": "mostly used for positive feedback"
    }
  },
  "required": ["type", "red", "yellow", "green"]
}`

	file, err := ioutil.TempFile(os.TempDir(), "schema.*.json")
	assert.NoError(err)

	defer os.Remove(file.Name())

	_, err = file.WriteString(schema)
	assert.NoError(err)

	tables := []struct {
		input    []byte
		expected string
	}{
		{
			[]byte("{}"),
			"not valid: (root): type is required / (root): red is required / (root): yellow is required / (root): green is required",
		},
		{
			[]byte(
				`{"type":"DeviceFeedback"}`,
			),
			"not valid: (root): red is required / (root): yellow is required / (root): green is required",
		},
	}

	for _, table := range tables {
		err := validate.Valid("file://"+file.Name(), table.input)
		assert.EqualError(err, table.expected)
	}
}
