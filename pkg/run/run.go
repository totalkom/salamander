package run

import (
	"io/ioutil"

	"github.com/rs/zerolog/log"
	"gitlab.com/wobcom/salamander/pkg/decode"
)

func Execute(decoderPath, jsonPath string) {
	dcdr, err := ioutil.ReadFile(decoderPath)
	if err != nil {
		log.Fatal().Msg(err.Error())
	}

	decoder := string(dcdr)

	data, err := ioutil.ReadFile(jsonPath)
	if err != nil {
		log.Fatal().Msg(err.Error())
	}

	if err := decode.Run(decoder, data); err != nil {
		log.Error().Caller().Msg(err.Error())
	}
}
