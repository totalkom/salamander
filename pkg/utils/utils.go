package utils

import (
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"sync"
	"time"

	"github.com/buger/jsonparser"
	"github.com/rs/zerolog/log"
	"github.com/tidwall/pretty"
)

// PrettyJSONPrint is printing JSON in pretty.
func PrettyJSONPrint(msg []byte) {
	fmt.Print(
		string(
			pretty.Color(
				pretty.Pretty(msg), nil,
			),
		),
	)
}

// SavePrettyJSON saves pretty JSON.
func SavePrettyJSON(msg []byte, payload string, dir string, wg *sync.WaitGroup) error {
	defer wg.Done()

	devEUI, err := jsonparser.GetString(msg, "devEUI")
	if err != nil {
		log.Error().Caller().Msg(err.Error())

		return err
	}

	fileName := fmt.Sprintf(
		"%s/%s-%s.json",
		dir,
		time.Now().Format("20060102150405.000"),
		devEUI,
	)

	r := make(map[string]interface{})
	if err := json.Unmarshal(msg, &r); err != nil {
		return err
	}

	o := struct {
		Payload string                 `json:"_payload"`
		Data    map[string]interface{} `json:"_data"`
	}{
		payload,
		r,
	}

	j, err := json.Marshal(o)
	if err != nil {
		return err
	}

	data := pretty.Pretty(j)

	err = ioutil.WriteFile(fileName, data, 0o600)
	if err != nil {
		return err
	}

	return nil
}

// Base64ToHexString decodes a Base64 and encodes it to hex.
func Base64ToHexString(b64 string) (string, error) {
	p, err := base64.StdEncoding.DecodeString(b64)
	if err != nil {
		return "", err
	}

	return hex.EncodeToString(p), nil
}

func Base64Decode(b64 string) ([]byte, error) {
	p, err := base64.StdEncoding.DecodeString(b64)
	if err != nil {
		return []byte{}, err
	}

	return p, nil
}
