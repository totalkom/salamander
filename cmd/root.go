//nolint: gochecknoglobals,gochecknoinits
package cmd

import (
	"os"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
)

var (
	Decoder     string
	JSONFile    string
	JSONOutput  bool
	LogDir      string
	MQTTChannel []string
	MQTTServer  string
	Password    string
	Username    string
	TestDir     string
)

var rootCmd = &cobra.Command{
	Use:   "salamander",
	Short: "salamander helps to debug iot stuff",
	PersistentPreRun: func(cmd *cobra.Command, args []string) {
		if JSONOutput {
			log.Logger = zerolog.New(os.Stderr).With().Timestamp().Logger()
		}
	},
	Run: func(cmd *cobra.Command, args []string) {
		if err := cmd.Help(); err != nil {
			log.Fatal().Msg(err.Error())
		}
		os.Exit(1)
	},
}

func init() {
	rootCmd.PersistentFlags().BoolVarP(&JSONOutput, "json-output", "j", false, "program output in JSON format")
}

func Execute() {
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})

	if err := rootCmd.Execute(); err != nil {
		log.Fatal().Msg(err.Error())
	}
}
