//nolint:gochecknoglobals,gochecknoinits
package cmd

import (
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"gitlab.com/wobcom/salamander/pkg/run"
)

func init() {
	rootCmd.AddCommand(runCmd)

	runCmd.Flags().StringVarP(&Decoder, "decoder", "d", "", "Decoder")

	if err := runCmd.MarkFlagRequired("decoder"); err != nil {
		log.Fatal().Msg(err.Error())
	}

	runCmd.Flags().StringVarP(&JSONFile, "json-file", "f", "", "JSON with log data")
}

var runCmd = &cobra.Command{
	Use:   "run",
	Short: "Run decoder against a recorded payload",
	Run: func(cmd *cobra.Command, args []string) {
		run.Execute(Decoder, JSONFile)
	},
}
