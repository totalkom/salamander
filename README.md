# salamander

[![wobcom](./wobcom-badge.png)](https://gitlab.com/wobcom)

`salamander` tries to help with debugging lora payloads with help of MQTT.

- Simple subscribe to MQTT channels
- Defining a JS-decoder to run against a device payload data
- Storing recieved payloads in JSON
- A Testrunner for that matches expected JSON after decoding the payload

## TODO

- [ ] Relive-Mode: Using JSON logs to replay data input. For faster decoder testing.

## Installation

### Binaries

They are available [here](https://gitlab.com/wobcom/salamander/-/releases).

### Docker

`docker pull registry.gitlab.com/wobcom/salamander:latest`

### Homebrew

Its possible to install salamander through our [taps](https://gitlab.com/wobcom/homebrew-iot).

```
brew tap wobcom/iot https://gitlab.com/wobcom/homebrew-iot.git
brew install salamander
```

## Usage

### Printing MQTT channel data

        salamander live --mqtt-server tls://iot.server.tld:8883 --mqtt-channel "application/+/device/+/rx" --user myuser --password mypassword

**Multiple channel subcriptions are allowed.**

### Saving data-logs

        salamander live --mqtt-server tls://iot.server.tld:8883 --mqtt-channel "application/+/device/+/rx" --user myuser --password mypassword --log-dir ./logs/

### Live testing decoder

        salamander live --mqtt-server tls://iot.server.tld:8883 --mqtt-channel "application/+/device/70b3d532600082a6/rx" --user myuser --password mypassword --decoder decoder.js

A `decoder.js` could look like this:

        function Decode(fPort, bytes) {
            if (bytes[0] != 2) {
              throw Error("in wrong mode");
            };
            var decoded = {};
            decoded.green = bytes[5] << 8 | bytes[6];
            decoded.yellow = bytes[7] << 8 | bytes[8];
            decoded.red = bytes[9] << 8 | bytes[10];
            return decoded;
        }

### Testrunner

This is used to run automated testing of payload decoders. It takes a directory with directories of payload decoders.

        $ /bin/ls -l
        total 32
        drwxrwxr-x 3 marv marv 4096 Aug 17 11:27 005-NKE_INO-001
        drwxrwxr-x 3 marv marv 4096 Aug 17 11:31 021-smart-traffic-stadtwob-salamander_3
        drwxrwxr-x 3 marv marv 4096 Aug 17 11:34 024_DAVIS_Vantage_Pro2
        drwxrwxr-x 3 marv marv 4096 Aug 17 11:35 027skiply_smilio
        drwxrwxr-x 3 marv marv 4096 Aug 17 11:37 027skiply_smilio_action
        -rw-rw-r-- 1 marv marv 7750 Aug  5 09:31 main.go
        -rw-rw-r-- 1 marv marv   42 Aug 17 11:38 Makefile

It needs a `testdata` directory with JSON test data in it. For example `005-NKE_INO-001/testdata/closed.json`.

        {
          "_expected": { "value": "closed", "type": "Device" },
          "_payload": "110a000f00551000",
          "_data": {
            "adr": true,
            "applicationID": "554",
            "applicationName": "021-smart-traffic-stadtwob-salamander",
            "data": "EQoADwBVEAA=",
            "devEUI": "70b3d5e75e008906",
            "deviceName": "NKE_1",
            "fCnt": 36,
            "fPort": 125,
            "object": {
              "data": {
                "pin_state": false
              },
              "lora": {
                "date": "2020-07-27T10:00:27.869Z",
                "payload": "110A000F00551000",
                "port": 125
              },
              "zclheader": {
                "attributID": "0x0055",
                "clusterdID": "0x000F",
                "cmdID": "0x0A",
                "endpoint": 0,
                "report": "standard"
              }
            },
            "rxInfo": [
              {
                "gatewayID": "00800000a0004632",
                "loRaSNR": -12,
                "location": {
                  "altitude": 91,
                  "latitude": 52.42803,
                  "longitude": 10.79167
                },
                "name": "Gateway_01_WNT",
                "rssi": -118,
                "time": "2020-07-27T10:00:26.562111Z",
                "uplinkID": "0728fa06-9277-443b-8268-22c642cb09c2"
              }
            ],
            "txInfo": {
              "dr": 2,
              "frequency": 868100000
            }
          }
        }

- `_expected`: The JSON that is expected to get returned after decoding the payload
- `_payload`: A HEX string of the Base64 decoded payload. In here for debugging
- `_data`: This is the raw data that gets sent over MQTT

Run the tests with

        $ salamander test -t ~/wip/chirpstack-payload-decoder

Output:

        1:38PM INF running... dir=005-NKE_INO-001 testFile=closed.json
        1:38PM INF running... dir=005-NKE_INO-001 testFile=opened.json
        1:38PM INF running... dir=021-smart-traffic-stadtwob-salamander_3 testFile=0.json
        1:38PM INF running... dir=021-smart-traffic-stadtwob-salamander_3 testFile=1.json
        1:38PM INF running... dir=021-smart-traffic-stadtwob-salamander_3 testFile=3.json
        1:38PM INF running... dir=021-smart-traffic-stadtwob-salamander_3 testFile=4.json
        1:38PM INF running... dir=021-smart-traffic-stadtwob-salamander_3 testFile=5.json
        1:38PM INF running... dir=021-smart-traffic-stadtwob-salamander_3 testFile=6.json
        1:38PM INF running... dir=021-smart-traffic-stadtwob-salamander_3 testFile=7.json
        1:38PM INF running... dir=024_DAVIS_Vantage_Pro2 testFile=pm0.json
        1:38PM INF running... dir=024_DAVIS_Vantage_Pro2 testFile=weather0.json
        1:38PM INF running... dir=027skiply_smilio testFile=0.json
        1:38PM INF running... dir=027skiply_smilio_action testFile=0.json

If everything is fine, it exists with exit code `0`.

## Misc

### Log-Filename-Format

`{year}{month}{day}{hours}{minutes}{seconds}.{milliseconds}-{dev-eui}.json`
