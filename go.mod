module gitlab.com/wobcom/salamander

go 1.15

require (
	github.com/buger/jsonparser v1.0.0
	github.com/davecgh/go-spew v1.1.1
	github.com/eclipse/paho.mqtt.golang v1.2.0
	github.com/pkg/errors v0.8.1
	github.com/robertkrimen/otto v0.0.0-20191219234010-c382bd3c16ff
	github.com/rs/zerolog v1.19.0
	github.com/spf13/cobra v1.0.0
	github.com/stretchr/testify v1.3.0
	github.com/tidwall/pretty v1.0.1
	github.com/xeipuuv/gojsonschema v1.2.0
	golang.org/x/net v0.0.0-20200707034311-ab3426394381 // indirect
	gopkg.in/sourcemap.v1 v1.0.5 // indirect
)
